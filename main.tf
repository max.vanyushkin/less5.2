resource "google_compute_instance" "juneway_vm" {
  count        = var.instance_count
  name         = format(var.machine_name, count.index + 1)
  machine_type = var.machine_type
  zone         = var.zone 

  tags = ["vanyushkinm", "juneway", "enable-http-s", "enable-udp"]

  metadata = {
    ssh-keys = "root:${file(var.public_key_path)}"
  }

  provisioner "local-exec" {
    command = "echo ${self.name} ${self.network_interface[0].access_config[0].nat_ip} >> host.list"
  }

  connection {
    type = "ssh"
    user = "root"

    private_key = file(var.private_key_path)
    host        = self.network_interface[0].access_config[0].nat_ip
  }


  provisioner "remote-exec" {
    inline = [
      "apt -y install nginx",
      "echo Juneway ${self.name} ${self.network_interface[0].access_config[0].nat_ip} > /var/www/html/index.nginx-debian.html"
    ]
  }

  boot_disk {
    initialize_params {
      image = var.image
      size  = var.disk_size
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}

resource "google_compute_firewall" "enable-http-s" {
  name        = "enable-http-s"
  network     = "default"
  description = "Creates firewall rule targeting tagged instances"
  priority    = 1
  source_ranges = ["0.0.0.0/0"]
  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }

  allow {
    protocol = "udp"
    ports    = ["10000-20000"]
  }

  source_tags = ["enable-http-s"]
}

resource "google_compute_firewall" "enable-udp" {
  name        = "enable-udp"
  network     = "default"
  description = "Creates firewall rule targeting tagged instances"
  priority    = 1
  source_ranges = ["10.0.0.23"]

  allow {
    protocol = "udp"
    ports    = ["10000-20000"]
  }

  source_tags = ["enable-udp"]
}
 