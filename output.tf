output "hosts" {
  value       = google_compute_instance.juneway_vm[*].name
  description = "The host value"
}

data "google_project" "project" { }

output "project_id" {
  value = data.google_project.project.number
}